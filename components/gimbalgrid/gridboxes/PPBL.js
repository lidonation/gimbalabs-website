import React from "react";
import Link from "next/link";

export default function PPBL() {
  return (
    <div className="h-full grid grid-cols-3 p-3 justify-center items-center">
      <div className="col-span-2 p-3">
        <h1 className="text-2xl">Live Now: Plutus Project-Based Learning 2023</h1>
        <h2 className="py-1"><a href="https://plutuspbl.io" target="_blank">Sign Up!</a></h2>
      </div>
    </div>
  );
}
