import React from "react";
import VoltairePage from "../../components/voltaire/VoltairePage";
import Layout from "../../components/Layouts/Layout";

export default function Index() {
  return (
    <Layout footerBg="bg-black2-900">
      <VoltairePage />
    </Layout>
  );
}
