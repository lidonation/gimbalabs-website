import React from "react";
import Layout from "../../components/Layouts/Layout";
import CarnivalPage from "../../components/carnival/CarnivalPage.js";

export default function Carnival() {
  return (
    <Layout>
      <CarnivalPage />
    </Layout>
  );
}
